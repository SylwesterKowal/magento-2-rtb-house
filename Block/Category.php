<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RTBHouse\Block;

class Category extends \Kowal\RTBHouse\Block\Main
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\RTBHouse\Helper\Settings $settings
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\RTBHouse\Helper\Settings                  $settings,
        \Magento\Customer\Model\Session                  $customerSession,
        \Magento\Framework\Registry                      $registry,
        array                                            $data = []
    )
    {
        $this->settings = $settings;
        $this->registry = $registry;
        $this->customerSession = $customerSession;
        parent::__construct($context, $settings, $customerSession, $data);
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->registry->registry('current_category')->getId();
    }

}

