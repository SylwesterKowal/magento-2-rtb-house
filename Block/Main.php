<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RTBHouse\Block;

class Main extends \Magento\Framework\View\Element\Template
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\RTBHouse\Helper\Settings $settings
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\RTBHouse\Helper\Settings                  $settings,
        \Magento\Customer\Model\Session                  $customerSession,
        array                                            $data = []
    )
    {
        $this->settings = $settings;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function display()
    {
        if ($this->settings->isEnabled()) {
            return true;
        } else {
            return false;
        }

    }

    public function getContent()
    {
        if ($main_code = $this->settings->getGeneralCfg('main_code')) {
            return $main_code;
        } else {
            return "";
        }
    }

    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    public function getUID()
    {
        if ($uid = $this->customerSession->getCustomer()->getId()) {
            return $uid;
        } else {
            return 'unknown';
        }

    }
}

