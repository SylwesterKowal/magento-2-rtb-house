<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RTBHouse\Block;

class OrderConrmationCode extends \Kowal\RTBHouse\Block\Main
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\RTBHouse\Helper\Settings $settings
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\RTBHouse\Helper\Settings                  $settings,
        \Magento\Customer\Model\Session                  $customerSession,
        \Magento\Checkout\Model\Session                  $checkoutSession,
        \Magento\Sales\Model\OrderFactory                $orderFactory,
        array                                            $data = []
    )
    {
        $this->settings = $settings;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        parent::__construct($context, $settings, $customerSession, $data);
    }

    public function getIncrementOrderId()
    {
        $order = $this->orderFactory->create()->loadByIncrementId($this->checkoutSession->getLastRealOrderId());
        return $order->getIncrementId();

    }

    public function getOrderSkus()
    {
        $skus = [];
        if ($this->checkoutSession->getLastRealOrderId()) {
            $order = $this->orderFactory->create()->loadByIncrementId($this->checkoutSession->getLastRealOrderId());
            $items = $order->getAllItems();

            foreach ($items as $item) {
                $skus[] = $item->getSku();
            }
        }
        return "'" . implode("','", $skus) . "'";;
    }

    public function getGrandTotal()
    {
        $order = $this->orderFactory->create()->loadByIncrementId($this->checkoutSession->getLastRealOrderId());
        return $order->getGrandTotal();
    }

}

