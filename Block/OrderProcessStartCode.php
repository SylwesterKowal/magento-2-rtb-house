<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RTBHouse\Block;

class OrderProcessStartCode extends \Kowal\RTBHouse\Block\Main
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\RTBHouse\Helper\Settings $settings
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Checkout\Model\Cart $cart
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\RTBHouse\Helper\Settings                  $settings,
        \Magento\Customer\Model\Session                  $customerSession,
        \Magento\Framework\Registry                      $registry,
        \Magento\Checkout\Model\Session                  $session,
        array                                            $data = []
    )
    {
        $this->settings = $settings;
        $this->registry = $registry;
        $this->customerSession = $customerSession;
        $this->session = $session;
        parent::__construct($context, $settings, $customerSession, $data);
    }

    /**
     * @return void
     * https://magento.stackexchange.com/questions/111137/magento-2-how-to-get-all-items-in-cart
     */
    public function getCartItems()
    {
        return $this->session->getQuote()->getAllItems();
    }
}

