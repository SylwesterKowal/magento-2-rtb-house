<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\RTBHouse\Block;

class SearchResultPageCode extends \Kowal\RTBHouse\Block\Main
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\RTBHouse\Helper\Settings $settings
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Block\Product\ListProduct $listProduct
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\RTBHouse\Helper\Settings                  $settings,
        \Magento\Customer\Model\Session                  $customerSession,
        \Magento\Framework\Registry                      $registry,
        \Magento\Catalog\Block\Product\ListProduct       $listProduct,
        array                                            $data = []
    )
    {
        $this->settings = $settings;
        $this->registry = $registry;
        $this->listProduct = $listProduct;
        $this->customerSession = $customerSession;
        parent::__construct($context, $settings, $customerSession, $data);
    }


    public function getSkus()
    {
        $collection = $this->listProduct->getLoadedProductCollection();
        $skus = [];
        $count = 0;
        foreach ($collection as $product) {
            $skus[] = $product->getSku();
            $count++;
            if ($count > 5) break;
        }
        return $skus;
    }


}

