# Mage2 Module Kowal RTBHouse

    ``kowal/module-rtbhouse``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
RTB House Events

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_RTBHouse`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-rtbhouse`
 - enable the module by running `php bin/magento module:enable Kowal_RTBHouse`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (rtbhouse/settings/enable)

 - uid (rtbhouse/settings/uid)

 - new_products_category_id (rtbhouse/settings/new_products_category_id)

 - sale_category_id (rtbhouse/settings/sale_category_id)

 - main_code (rtbhouse/settings/main_code)


## Specifications

 - Block
	- Main > main.phtml

 - Block
	- Homepage > homepage.phtml

 - Block
	- Category > category.phtml

 - Block
	- CategorySale > categorysale.phtml

 - Block
	- CategoryNews > categorynews.phtml

 - Block
	- ProductPageCode > productpagecode.phtml

 - Block
	- WishlistPageCode > wishlistpagecode.phtml

 - Block
	- SearchResultPageCode > searchresultpagecode.phtml

 - Block
	- AddToCartCode > addtocartcode.phtml

 - Block
	- OrderProcessStartCode > orderprocessstartcode.phtml

 - Block
	- OrderConrmationCode > orderconrmationcode.phtml

 - Helper
	- Kowal\RTBHouse\Helper\Settings


## Attributes



